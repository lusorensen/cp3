<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo ("helo");
}

$stmt = $mysqli->prepare("select id, type, title, src, text, posted FROM posts WHERE username=?");

if(!$stmt){
	echo("error");
}

// Bind the parameter
$stmt->bind_param('s', $username);
$username= $_SESSION['other'];
$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){
    
	$id = htmlentities($row["id"]);
	$type = htmlentities($row["type"]);
	$title = htmlentities($row["title"]);
    $src= htmlentities($row["src"]);
    $posted= htmlentities($row["posted"]);
    $text= htmlentities($row["text"]);
    
    if ($type=='video') {
        
        echo "<br>
        <h3> $title </h3>
        <br>
        <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/$src\" frameborder=\"0\" allowfullscreen></iframe>
        <br>
        <p>$text</p>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
             
    }
    
    if ($type=='text') {
        
        echo "<br>
        <h3> $title </h3>
        <br>
        <p> $text </p>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
    if ($type=='image'){
        
         echo "<br>
        <h3> $title </h3>
        <br>
        <img src=\"Users/$username/$src\" alt=$src>
        <br>
        <p> $text </p>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
    if ($type=='link'){
        
         echo "<br>
        <h3> <a href=\"$src\" target=\"_blank\">$title</a> </h3>
        <br>
        <p> $text </p>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
}

$stmt->close();

?>

