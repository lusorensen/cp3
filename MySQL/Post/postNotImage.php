<?php

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');

if($mysqli->connect_errno) {
    echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}

if(empty($_SESSION['username'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Must be logged in"
	));
	exit;
}


$username = $_SESSION['username'];
$type= $_POST["type"];
$title= $_POST["title"];
$text= $_POST["text"];

if($type=='video'){
    
    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $_POST["src"], $matches);
    $src= $matches[1];
    
} else{
    
    $src= $_POST["src"];    
}

$stmt = $mysqli->prepare("insert into posts (username, type, title, text, src) values (?, ?, ?, ?, ?)");

if(!$stmt){
    
echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}   

$stmt->bind_param('sssss', $username, $type, $title, $text, $src);

$stmt->execute();

$stmt->close();

echo json_encode(array(
		"success" => true,
		"message" => $_POST["src"]
	));
exit;



?>