<?php

ini_set("session.cookie_httponly", 1);
session_start();

// Get the filename and make sure it is valid
$filename = basename($_FILES['imageFile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
    echo "wrong file type";
    exit;

}

if(empty($_SESSION['username'])){
    echo "Please Log In";
    exit;

}

$username = $_SESSION['username'];
 
$full_path = sprintf("/home/lsorensen/public_html/Final/Users/%s/%s", $username, $filename);

if(move_uploaded_file($_FILES['imageFile']['tmp_name'], $full_path)){
    
    $mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
    if($mysqli->connect_errno) {
        echo "couldn't connect to database";
        exit;
    }
    
    $username = $_SESSION['username'];
    $type= 'image';
    $title= $_POST["imageTitle"];
    $text= $_POST["imageText"];
    $src= $filename;

    $stmt = $mysqli->prepare("insert into posts (username, type, title, text, src) values (?, ?, ?, ?, ?)");
    
    if(!$stmt){
    echo "could no be added to table";
    }   


    $stmt->bind_param('sssss', $username, $type, $title, $text, $src);
 
    $stmt->execute();
 
    $stmt->close();
    
    echo "success?";
    
    
}else{
    echo $full_path;
    exit;
}

?>