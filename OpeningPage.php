<!DOCTYPE html>
<head>
<title>Log In</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<script>
		
//makes login interface appear    	
function logInForm(){
    $.ajax({url: "html/OpeningPage/loginForm.html", success: function(result){
        $("#btn").html(result);
    }});
}

//makes register interface appear
function registerForm(){
    $.ajax({url: "html/OpeningPage/registerForm.html", success: function(result){
        $("#btn").html(result);
    }});
}

//resets home page
function restore() {
    $.ajax({url: "html/OpeningPage/HomeButtons.html", success: function(result){
        $("#btn").html(result);
    }});
}

//post username and password from login forms
//if success redirect to user page
function logIn() {        
	var usr = $("#username").val();
	var psw = $("#password").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Login/Login.php', data: {"username": usr, "password": psw}, dataType: 'json', success: function(json) {
		if (json.success) {
			alert(json.message);
			window.location.href = "UserPage.php";
		}else{	
			alert(json.message);
		}
	}});	
	
}

//post username, password and password2 for validation
//if successful register user and create folder in 
function register() {    
	var usr = $("#rusername").val();
	var psw = $("#rpassword").val();
	var psw2 = $("#rpassword2").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Login/Register.php', data: {"newusername": usr, "password": psw , "password2": psw2},
	dataType: 'json', success: function(json) {
		if (json.success) {
			alert(json.message);
			window.location.href = "UserPage.php";
		}else{	
			alert(json.message);
		}
	}});	

}

</script>

<style>
    
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
    background-color: #B4C9ED;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
}

button {
  border-radius: 30px;
  font-family: "Lucida Console", Monaco, monospace;
  color: #ffffff;
  font-size: 31px;
  background: #3498db;
  padding: 15px 20px 15px 20px;
  text-decoration: none;
  border-width:0px
  
}

button:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

button:focus {
	outline:0;
}

input[type=button] {
    
border-radius: 20px;
  font-family: "Lucida Console", Monaco, monospace;
  color: #ffffff;
  font-size: 20px;
  background: #3498db;
  padding: 10px 15px 10px 15px;
  text-decoration: none;
  border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none; 
}

input[type=button]:focus {
	outline:0;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}



</style>


</head>
<body>

<h1 align=center> Share Space </h1>

<div id= "btn" align="center">
    
<button id="loginBtn" onclick= "logInForm()">Login</button>
&nbsp; &nbsp;
<button id="registerBtn" onclick= "registerForm()">Register</button>
    
</div>
        
        
</body>
</html>