//show buttons for image, link, video or text option	
function restoreUser() {
	$.ajax({url: "html/UserPage/homeButtons.html", success: function(result){
        $("#upload").html(result);
    }});
}

//show image upload form 
function imagePostForm() {
	$.ajax({url: "html/UserPage/imagePostForm.html", success: function(result){
        $("#upload").html(result);
    }});
}

//show text post upload form
function textPostForm() {
	$.ajax({url: "html/UserPage/textPostForm.html", success: function(result){
        $("#upload").html(result);
    }});
}

//show link upload form
function linkPostForm() {
	$.ajax({url: "html/UserPage/linkPostForm.html", success: function(result){
        $("#upload").html(result);
    }});
}

//show video upload form
function videoPostForm() {
	$.ajax({url: "html/UserPage/videoPostForm.html", success: function(result){
        $("#upload").html(result);
    }});
}

//makes customize interface appear    	
function customizeForm(){
    $.ajax({url: "html/UserPage/customizeForm.html", success: function(result){
        $("#upload").html(result);
		placeValues();
    }});
}

//sets customize interface to current page values
function placeValues() {
	
	$.ajax({url: 'MySQL/Custom/getCustom.php', dataType: 'json', success: function(json) {
		
		if (json.success) {
			
			$("#ttl").val(json.title);
			$("#bgrColor").val(json.bgrColor);
			$("#hrColor").val(json.hrColor);
			$("#typeface").val(json.typeface);
			$("#ttlColor").val(json.ttlColor);
			$("#txtColor").val(json.txtColor);
			
		}else{
			
			alert(json.message);
		}
	}});
}
