function preview() {
	
	var ttl = $("#ttl").val();
	var bgrColor = $("#bgrColor").val();
	var hr = $("#hrColor").val();
	var typeface = $("#typeface").val();
	var ttlColor = $("#ttlColor").val();
	var txtColor = $("#txtColor").val();
	
	//title
	$("#title").html(ttl);
	
	//background color
	$("body").css("background-color", bgrColor);

	//hr color
	$("hr").css("color", hr);
	
	//typeface
	$("body").css("font-family",typeface);
	
	//title color
	$("h1").css("color", ttlColor);
	
	//text color
	$("body").css("color", txtColor);

}

function changeCustom() {
	
	var ttl = $("#ttl").val();
	var bgrColor = $("#bgrColor").val();
	var hrColor = $("#hrColor").val();
	var typeface = $("#typeface").val();
	var ttlColor = $("#ttlColor").val();
	var txtColor = $("#txtColor").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Custom/changeCustom.php',
	data: {"ttl": ttl,
	"bgrColor": bgrColor,
	"hrColor": hrColor,
	"typeface": typeface,
	"ttlColor": ttlColor,
	"txtColor": txtColor},
	dataType: 'json', success: function(json) {
		
		if (json.success) {
			
			alert("success!")
			renderCustom();
			restoreUser();
			
		}else{
			
			alert(json.message);
			
		}
	}});
}

function renderCustom() {
	
	$.ajax({url: 'MySQL/Custom/getCustom.php', dataType: 'json', success: function(json) {
		
		if (json.success) {
			
			//title
			$("#title").html(json.title);
			
			//background color
			$("body").css("background-color", json.bgrColor);
			
			//typeface
			$("body").css("font-family", json.typeface);
			
			//title color
			$("h1").css("color", json.ttlColor);
			
			//text color
			$("body").css("color", json.txtColor);
			
			//button color
			$("hr").css("color", json.hrColor);
			
		}else{
			
			alert(json.message);
		}
	}});
}

function clearStyle() {
	
	renderCustom();
	placeValues();

	
}
