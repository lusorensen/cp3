//post text form data to mysql data base
function postText() {
	var ttl = $("#textTitle").val();
	var txt = $("#textText").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Post/postNotImage.php', data: {"type": "text", "title": ttl, "text": txt},
	dataType: 'json', success: function(json) {
		if (json.success) {
			alert("Success");
			restoreUser();
			getFeed();
		}else{	
			alert(json.message);
		}
	}});
}

//post link form data to mysql data base
function postLink() {
	var src = $("#linkURL").val();
	var ttl = $("#linkTitle").val();
	var txt = $("#linkText").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Post/postNotImage.php', data: {"type": "link", "title": ttl, "text": txt, "src": src},
	dataType: 'json', success: function(json) {
		if (json.success) {
			alert("Success");
			restoreUser();
			getFeed();
		}else{	
			alert(json.message);
		}
	}});
}


function postVideo() {        
	var src = $("#videoURL").val();
	var ttl = $("#videoTitle").val();
	var txt = $("#videoText").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Post/postNotImage.php', data: {"type": "video", "title": ttl, "text": txt, "src": src},
	dataType: 'json', success: function(json) {
		if (json.success) {
			alert("Success");
			restoreUser();
			getFeed();
		}else{	
			alert(json.message);
		}
	}});		
}